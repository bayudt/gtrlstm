#GTRLSTM
An open-source framework for generating sentences from RDF Data.
###Example:

|                 |                                                                    |
|-----------------|--------------------------------------------------------------------|
|                 | <John Doe,birth place,London>                                      |
| RDF triples     | <John Doe,birth date,1967-01-10>                                   |
|                 | <London,capital of,England>                                        |
|                 |                                                                    |
| Target sentence | John Doe was born on 1967-01-10 in London, the capital of England. |                       	
 
 

#Content:
1. testing.ipynb
2. config.py
3. model_zoo.py
4. Readme.md
5. utility.py
6. data\
7. trained_models\


#Requirement:
1. nmt-keras (https://github.com/lvapeab/nmt-keras)
2. Stanford Parser
3. nltk
4. networkx
5. Gensim and glove pre-trained word embedding, copy to folder: ../word_vector/

#References
Bayu Distiawan Trisedya, Jianzhong Qi, Rui Zhang, Wei Wang. [**GTR-LSTM: A Triple Encoder for Sentence Generation from RDF Data.**](http://aclweb.org/anthology/P18-1151) ACL 2018.