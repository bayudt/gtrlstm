from dateutil.parser import parse
import re
import string
import requests
import json
import editdistance
import sys
import datetime
from nltk.corpus import wordnet as wn
from nltk import ngrams
from nltk.tokenize.moses import MosesTokenizer, MosesDetokenizer
from itertools import groupby
from nltk.tree import Tree
from nltk.tokenize import sent_tokenize
import urllib
from datefinder import DateFinder
import datefinder
from difflib import SequenceMatcher
from bs4 import BeautifulSoup
from kitchen.text.converters import getwriter, to_bytes, to_unicode
from kitchen.i18n import get_translation_object
from nltk import word_tokenize
from random import choice
import networkx as nx
translations = get_translation_object('example')
_ = translations.ugettext
b_ = translations.lgettext


import nltk
'''from nltk.parse.stanford import StanfordParser
from nltk.tag.stanford import StanfordNERTagger
english_parser = StanfordParser(model_path="edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz")
english_nertagger = StanfordNERTagger('english.all.3class.distsim.crf.ser.gz')'''

import gensim
from gensim.models import word2vec
w2v_model = gensim.models.KeyedVectors.load_word2vec_format("../word_vector/wiki.vector", binary=False)

regex = re.compile(".*?\((.*?)\)")
def removeBracket(string):
    result = re.findall(regex, string)
    for r in result:
        remove = '('+r+')'
        string = string.replace(remove, '')
    return string.replace('  ', ' ').strip()
    
def containBracket(string):
    if string == '<unk>':
        return string
    result = re.findall(regex, string)
    returned = None
    for r in result:
        returned = r
        break
    if returned == None:
        return returned
    else:
        return ' '.join(camel_case_split(returned)).lower()

url_regex = re.compile(
        r'^(?:http|ftp)s?://' # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
        r'localhost|' #localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
        r'(?::\d+)?' # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
def contains_URL(string):
    found_url = False
    string = string.split()
    for s in string:
        result = re.findall(url_regex, s)
        for r in result:
            return True
    return False

def normalizeDate(input_string):
    finder = DateFinder()
    x = finder.extract_date_strings(input_string)
    matches = list(datefinder.find_dates(input_string))
    if len(matches) > 0:
        for m in matches:
            txt_str = x.next()
            find = txt_str[0]
            replacement = str(m).split(' ')[0]
            today = str(datetime.date.today())
            if replacement.strip() == today.strip():
                continue
            if(len(find) > 4):
                input_string = input_string.replace(find, replacement)
    else:
        pass
    return input_string

def getSPO(triple):
    s = triple.s.replace("_", " ").replace('"', '').strip()
    p = triple.p.replace("_", " ").replace('"', '').strip()
    o = triple.o.replace("_", " ").replace('"', '').strip()
    return s,p,o

def replaceDuplicateSequence(string):
    data = string.split(' ')
    output = list()
    prev = ''
    for d in data:
        if d!=prev:
            prev = d
            output.append(d)
    return ' '.join(output)

def printData(data):
    triples, texts = data
    for t in triples:
        print t
    print texts

def getLabelFullTries(entity, position, dictionary=None, advance=True):
    label = getLabel(entity, position, dictionary, advance)
    if label == 'UNKOWN':
        label = position
    if label == position and ',' in entity:
        entities = entity.split(',')
        for e in entities:
            tmp = getLabel(e, position, dictionary, advance)
            label = tmp
            if label != position:
                break
    if label == position and ' and ' in entity:
        entities = entity.split(' and ')
        for e in entities:
            tmp = getLabel(e, position, dictionary, advance)
            label = tmp
            if label != position:
                break
    if label == position:
        label = getLabel(entity, position, dictionary, advance)
    return label
    
def getLabel(entity, position, dictionary=None, advance=True):
    label = position
    if dictionary and entity in dictionary:
        return dictionary[entity]
    #CammelCase on bracket
    insideBracket = getBracketContent(entity)
    if insideBracket != entity[:-1] and camel(insideBracket):
        label = insideBracket
    else:
        entity = entity.split('(')[0]
        #Number only
        if entity.isdigit() or isFloat(entity):
            label = "NUMBER"

        #DATE format
        elif isDate(entity):
            label = "DATE"
        
        #FROM DICTIONARY or DBPEDIA
        else:
            #FROM DICT
            if dictionary:
                try:
                    label = dictionary[entity].strip()
                except:
                    pass
            #FROM ONLINE
            else:
                #print entity
                #print "FROM DBPEDIA"
                fromDBpedia = getDBpedia(entity, advance)
                if fromDBpedia:
                    label = fromDBpedia
                else:
                    #print "SEARCH", entity, "FROM GOOGLE"
                    fromGoogle = getGoogle(entity, advance)
                    if fromGoogle:
                        label = fromGoogle
    return label

def camel(s):
    return (s[0] != s[0].upper() and s != s.lower() and s != s.upper())

def camel_case_split(identifier):
    matches = re.finditer('.+?(?:(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|$)', identifier)
    return [m.group(0) for m in matches]

def getBracketContent(s):
    return s[s.find("(")+1:s.find(")")]

def removeNonEntityBracket(s):
    s = s.replace("-LRB-", "(").replace("-RRB-", ")")
    content = s[s.find("(")+1:s.find(")")]
    if "ENTITIES_" not in content and "PREDICATE_" not in content:
        return s.replace("(" +content+")", "")
    else:
        return s

def isDate(string):
    try: 
        parse(string)
        return True
    except ValueError:
        return False
    
def isFloat(s):
    try:
        float(s)
        return True
    except ValueError:
        return False
    
def longest_common_substring(s1, s2):
    m = [[0] * (1 + len(s2)) for i in xrange(1 + len(s1))]
    longest, x_longest = 0, 0
    for x in xrange(1, 1 + len(s1)):
        for y in xrange(1, 1 + len(s2)):
            if s1[x - 1] == s2[y - 1]:
                m[x][y] = m[x - 1][y - 1] + 1
                if m[x][y] > longest:
                    longest = m[x][y]
                    x_longest = x
            else:
                m[x][y] = 0
    return s1[x_longest - longest: x_longest]

def getNameFromURI(uri):
    return uri.split('/')[-1].split('#')[-1].strip()

def getTypeDeep(classes):
    gen_type = 'ENTITY'
    gen_depth = sys.maxint
    all_type = set()
    for t in classes:
        if t['label'] == 'agent':
            continue
        synsets = wn.synsets(t['label'].replace(' ', '_'))
        if len(synsets) == 0 and ' ' in t['label']:
            synsets = wn.synsets(find_head_of_np(english_parser.raw_parse(t['label'].lower()).next()))
        for synset in synsets:
            if synset.name().split('.')[1] != 'n':
                continue

            if synset.min_depth() < gen_depth:
                gen_depth = synset.min_depth()
                gen_type = t['label']
            all_type.add(t['label'])
    max_score = 0
    choosen_type = ''
    for t in all_type:
        t_original = t
        if ' ' in t:
            t = find_head_of_np(english_parser.raw_parse(t.lower()).next())
        t = t.lower()
        if t != gen_type.lower():
            if gen_type.lower() in w2v_model.vocab and t in w2v_model.vocab:
                score = w2v_model.similarity(t, gen_type.lower())
                if score > max_score:
                    max_score = score
                    choosen_type = t_original
                    
    if gen_type!='ENTITY' and choosen_type != '':
        return gen_type.upper() + "[%%%]" + choosen_type.upper()
    elif gen_type != 'ENTITY':
        return gen_type.upper()
    else:
        return False

def getDBpedia(entity, advance=True):
    qstring = entity
    url = 'http://lookup.dbpedia.org/api/search.asmx/KeywordSearch?QueryString='+urllib.quote_plus(qstring.encode('utf8'))
    headers = {'accept': 'application/json'}

    r = requests.get(url, headers=headers)
    content = json.loads(r.text)['results']
    
    found = False
    max_distance = -1
    if len(content) > 0:
        choosen_content = None
        for c in content:
            if len(c['classes'])==0 and len(c['categories'])==0:
                continue
            tmp = SequenceMatcher(None, c['label'], qstring).ratio()
            if tmp > max_distance:
                max_distance = tmp
                choosen_content = c
        
        if choosen_content == None:
            return False
        
        label_class = 'ENTITY'
        if advance:
            if len(choosen_content['classes']) > 0:
                label_class = getTypeDeep(choosen_content['classes'])
                found = True
            else:
                label_class = getTypeDeep(choosen_content['categories'])
                found = True
        else:
            if len(choosen_content['classes']) > 0:
                    for kelas in choosen_content['classes']:
                        if (not kelas['label'].upper().startswith("OWL#")) and (not contains_URL(kelas['label'])):
                            label_class = kelas['label'].upper()
                            found = True
                            break
            else:
                if len(choosen_content['categories']) > 0:
                    for category in choosen_content['categories']:
                        if not category['label'].upper().startswith("OWL#"): 
                            label_class = category['label'].upper()
                            found = True
                            break
    if found:
        return label_class
    else:
        return False
    
def getEntityName(entity):
    url = "https://www.google.com.au/search?q="+urllib.quote_plus(entity.encode('utf8'))+" Wikipedia&start=0&num=1"
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    response = requests.get(url, headers=headers)
    content = response.content
    soup = BeautifulSoup(content, "lxml")
    urlList = list()
    for result in soup.findAll('h3', class_="r"):
        for url in result.findAll('a',href=True):
            urlList.append(url['href'].split('/')[-1].replace('_', ' '))
    return urlList[0].strip()

def getGoogle(entity, advance=True):
    try:
        qstring = entity
        url = 'https://kgsearch.googleapis.com/v1/entities:search?query='+urllib.quote_plus(qstring.encode('utf8'))+'&key=AIzaSyCSWIDmgxih5nX8ijMsT_QUdwtwnW0NyOo&limit=10&indent=True'
        print url
        r = requests.get(url)
        content = json.loads(r.text)['itemListElement']
        
        #try normalize name by using google search#
        if len(content)==0:
            qstring = getEntityName(entity)
            url = 'https://kgsearch.googleapis.com/v1/entities:search?query='+urllib.quote_plus(qstring.encode('utf8'))+'&key=AIzaSyCSWIDmgxih5nX8ijMsT_QUdwtwnW0NyOo&limit=10&indent=True'
            r = requests.get(url)
            content = json.loads(r.text)['itemListElement']
            
        found = False
        max_distance = 0
        if len(content) > 0:
            choosen_content = None
            for c in content:
                if 'name' not in c['result']:
                    continue
                tmp = SequenceMatcher(None, c['result']['name'], qstring).ratio()
                if tmp > max_distance:
                    max_distance = tmp
                    choosen_content = c
                ## Re-search again if match schore is low ##
                if max_distance < 0.5:
                    if len(content)==0:
                        qstring = getEntityName(entity)
                        url = 'https://kgsearch.googleapis.com/v1/entities:search?query='+urllib.quote_plus(qstring.encode('utf8'))+'&key=AIzaSyCSWIDmgxih5nX8ijMsT_QUdwtwnW0NyOo&limit=10&indent=True'
                        r = requests.get(url)
                        content = json.loads(r.text)['itemListElement']
                    max_distance = 0
                    if len(content) > 0:
                        choosen_content = None
                        for c in content:
                            if 'name' not in c['result']:
                                continue
                            tmp = SequenceMatcher(None, c['result']['name'], qstring).ratio()
                            if tmp > max_distance:
                                max_distance = tmp
                                choosen_content = c
                #END try normalize name by using google search#

            if choosen_content == None:
                return False
            
            label_class = 'ENTITY'
            classes = choosen_content['result']['@type']
            
            if advance:
                norm_classes = list()
                for c in classes:
                    if c == 'Thing':
                        continue
                    data = dict()
                    data['label'] = ' '.join(camel_case_split(c)).lower()
                    norm_classes.append(data)
                
                if len(classes) > 0:
                    label_class = getTypeDeep(norm_classes)
                    found = True
                else:
                    if 'description' in c['result']:
                        label_class = ' '.join(camel_case_split(choosen_content['result']['description'])).upper()
                        found = True
            else:
                if len(classes) > 0:
                        for kelas in classes:
                            label_class = ' '.join(camel_case_split(kelas)).upper()
                            found = True
                            break
                else:
                    if 'description' in c['result']:
                        label_class = ' '.join(camel_case_split(choosen_content['result']['description'])).upper()
                        found = True
            if (label_class == 'THING' or label_class == False) and 'description' in choosen_content['result']:
                label_class = ' '.join(camel_case_split(choosen_content['result']['description'])).upper()
                found = True
        if found:
            return label_class
        else:
            return False
    except:
        raise
        return False

def find_head_of_np(np):
    noun_tags = ['NN', 'NNS', 'NNP', 'NNPS']
    top_level_trees = [np[i] for i in range(len(np)) if type(np[i]) is Tree]
    ## search for a top-level noun
    top_level_nouns = [t for t in top_level_trees if t.label() in noun_tags]
    if len(top_level_nouns) > 0:
        ## if you find some, pick the rightmost one, just 'cause
        return top_level_nouns[-1][0]
    else:
        ## search for a top-level np
        top_level_nps = [t for t in top_level_trees if t.label()=='NP']
        if len(top_level_nps) > 0:
            ## if you find some, pick the head of the rightmost one, just 'cause
            return find_head_of_np(top_level_nps[-1])
        else:
            ## search for any noun
            nouns = [p[0] for p in np.pos() if p[1] in noun_tags]
            if len(nouns) > 0:
                ## if you find some, pick the rightmost one, just 'cause
                return nouns[-1]
            else:
                ## return the rightmost word, just 'cause
                return np.leaves()[-1]

def getTypeSubject(subject, predicate):
    subject = subject.lower()
    predicate = predicate.lower()
    
    if ' ' in predicate or predicate not in w2v_model.vocab:
        return subject
    if ' ' in subject or subject not in w2v_model.vocab:
        return subject
    
    synsets = wn.synsets(subject.replace(' ', '_'))
    if len(synsets) == 0 and ' ' in subject:
        synsets = wn.synsets(find_head_of_np(english_parser.raw_parse(subject.lower()).next()))
        
    possible_words = set()
    possible_words.add(subject)
    for synset in synsets:
        if synset.name().split('.')[1] != 'n':
            continue
        for hyper in synset.hypernyms():
            possible_words.add(hyper.name().split('.')[0])
    
    max_score = 0
    choosen_type = ''
    for word in possible_words:
        if word in w2v_model.vocab:
            score = w2v_model.similarity(word, predicate)
            if score > max_score:
                max_score = score
                choosen_type = word
    return choosen_type

def replaceSequence(sequence, replacement, lst, expand=False):
    out = list(lst)
    cont = True
    while cont:
        tmp_list = out
        out = list(tmp_list)
        for i, e in enumerate(tmp_list):
            if e.lower() == sequence[0].lower():
                i1 = i
                f = 1
                for e1, e2 in zip(sequence, tmp_list[i:]):
                    if e1.lower() != e2.lower():
                        f = 0
                        break
                    i1 += 1
                if f == 1:
                    del out[i:i1]
                    if expand:
                        for x in list(replacement):
                            out.insert(i, x)
                    else:
                        out.insert(i, replacement)
                    break
            if i >= len(out)-1:
                cont = False
    return out

def replaceAbbreviation(text, triples, abbr):
    regex_abbr = r"\b[A-Z][a-zA-Z\.]*[A-Z]\b\.?"
    regex_punct = re.compile('[%s]' % re.escape(string.punctuation))
    
    #REPLACE TEXT#
    all_abbrev = re.findall(regex_abbr, text)
    all_abbrev2 = re.findall(regex_abbr, regex_punct.sub('',text))
    all_abbrev += all_abbrev2
    
    for abbrev in all_abbrev:
        abbrev = _(abbrev)
        if abbrev in abbr:
            for triple in triples:
                for t in triple:
                    longest_abbr = abbrev
                    for a in abbr[abbrev]:
                        a = _(a)
                        if a in _(t[0]) and (len(a) > len(longest_abbr) and ' ' in a):
                            longest_abbr = a
                    text = text.replace(abbrev, longest_abbr)
    #END REPLACE TEXT#
    
    #REPLACE TRIPLE#
    for triple in triples:
        for t in triple:
            all_abbrev = re.findall(regex_abbr, t[0])
            for abbrev in all_abbrev[:1]:#Take only the first occurence
                abbrev = _(abbrev)
                if abbrev in abbr:
                    longest_abbr = abbrev
                    for a in abbr[abbrev]:
                        a = _(a)
                        if a in _(text) and (len(a) > len(longest_abbr) and ' ' in a):
                            longest_abbr = a
                    t[0] = t[0].replace(abbrev, longest_abbr)
    #END REPLACE TRIPLE#
    return text, triples

def replaceString(text, triples):
    text = normalizeDate(text)
    #Populate subject & object
    replacement = dict()
    keys = dict()
    for triple in triples:
        s,p,o = triple
        replace = (s,o)
        for data in replace:
            key, value, label = data
            key = removeBracket(key)
            value = removeBracket(value)
            replacement[key] = value
            keys[value] = key
    
    #REPLACE EXACT MATCH, FIND THE LONGEST ONE FIRST#
    entities_found = set()
    tokenizer = MosesTokenizer()
    detokenizer = MosesDetokenizer()
    out = ' '.join(nltk.word_tokenize(text))
    out = tokenizer.tokenize(out)
    for k in sorted(replacement, key=lambda k: len(k), reverse=True):
        k2 = ' '.join(nltk.word_tokenize(k))
        tmp = replaceSequence(tokenizer.tokenize(k2), replacement[k], out)
        if tmp != out:
            out = tmp
            entities_found.add(k)
    for idx, o in enumerate(out):
        out[idx] = (detokenizer.detokenize([out[idx]]))[0]
    text = ' '.join(out)
    #Remove Replacement
    for e in entities_found:
        del replacement[e]
    #END REPLACE EXACT MATCH, FIND THE LONGEST ONE FIRST#
    
    #REPLACE PARTIAL MATCH#
    if len(replacement) > 0:
        result = ''
        ## Using N-gram
        entities_found = set()
        out = text.split()
        for data in replacement:
            find = data
            label = replacement[data]
            
            N = len(tokenizer.tokenize(find))
            n_grams = list()
            out = out
            for gram in ngrams(out, N):
                n_grams.append(gram)
            
            for subtree in n_grams:
                sub_text = ' '.join(subtree)
                sim = SequenceMatcher(None, sub_text.lower(), find.lower()).ratio()
                
                if sim >= 0.9:
                    tmp = replaceSequence(subtree, label, detokenizer.detokenize(out))
                    if tmp != detokenizer.detokenize(out):
                        out = tmp
                        entities_found.add(data)
                        
        for idx, o in enumerate(out):
            out[idx] = (detokenizer.detokenize([out[idx]]))[0]
        text = ' '.join(out)
        for e in entities_found:
            del replacement[e]
        
        ## Using PARSER
        sentences = sent_tokenize(text)
        for sent in sentences:
            results_parser = english_parser.raw_parse(sent)
            tree = next(results_parser)

            for data in sorted(replacement, key=lambda k: len(k), reverse=True):
                find = data
                label = replacement[data]
                stree = None

                min_distance = sys.maxint
                for subtree in tree.subtrees():
                    sub_text = ' '.join(subtree.leaves())
                    if 'ENTITIES_' not in sub_text:
                        comparator = longest_common_substring(find.replace(' ', '').lower(), sub_text.replace(' ', '').lower())
                        tmp = editdistance.eval(sub_text, comparator)
                        tmp = editdistance.eval(find, comparator)
                        tmp += editdistance.eval(sub_text, find)
                        tmp /= 3.0
                        if tmp < min_distance:
                            min_distance = tmp
                            choosen_comparator = comparator
                            stree = subtree
                    else:
                        contained_entity = None
                        for idx, k in enumerate(sub_text.split()):
                            if k in keys:
                                contained_entity = keys[k]
                                break
                        if contained_entity.lower() in find.lower() and (float(len(contained_entity))/len(find) < float(1)/5):
                            comparator = longest_common_substring(find.replace(' ', '').lower(), sub_text.replace(' ', '').lower())
                            tmp = editdistance.eval(sub_text, comparator)
                            tmp = editdistance.eval(find, comparator)
                            tmp += editdistance.eval(sub_text, find)
                            tmp /= 3.0
                            if tmp < min_distance:
                                min_distance = tmp
                                choosen_comparator = comparator
                                stree = subtree
                        
                if stree != None:
                    if ' ' not in ' '.join(stree.leaves()) and ' ' not in find:
                        tmp = SequenceMatcher(None, ' '.join(stree.leaves()), find).ratio()
                        if tmp >= 0.7:
                            for i in range(0, len(stree)):
                                stree[i] = label
                    else:
                        if float(len(choosen_comparator))/len(find) >= float(1)/5:
                            for i in range(0, len(stree)):
                                stree[i] = label

            result += (removeNonEntityBracket(replaceDuplicateSequence(' '.join(tree.leaves()))) + ' ').replace('  ', ' ')
    else:
        result = removeNonEntityBracket(replaceDuplicateSequence(text)).replace('  ', ' ')
    #END REPLACE PARTIAL MATCH#
    
    ### REMOVE LOCATION THAT NOT RECORDED IN TRIPLES ###
    ner_result = english_nertagger.tag(result.split())
    
    remove_location = list()
    for tag, chunk in groupby(ner_result, lambda x:x[1]):
        if tag == "LOCATION":
            remove_location.append(" ".join(w for w, t in chunk))
    #print remove_location
    parser_result = english_parser.raw_parse(result)
    tree = next(parser_result)
    if(len(remove_location) > 0):        
        for r in remove_location:
            for s in tree.subtrees(lambda t: t.label() == "PP" or t.label() == "NNP" or t.label() == "NP"):
                sub_text = ' '.join(s.leaves())
                if ("ENTITIES_" not in sub_text and r in sub_text):
                    for i in range(0, len(s)):
                        s[i] = ''
        result = replaceDuplicateSequence(' '.join(tree.leaves())).replace('  ', ' ')
    ### END REMOVE LOCATION THAT NOT RECORDED IN TRIPLES ###
    
    ### REMOVE UNKOWN NUMBER ###
    for s in tree.subtrees(lambda t: t.label() == "PP" or t.label() == "CD"):
        sub_text = ' '.join(s.leaves())
        if("ENTITIES_" not in sub_text and len(list(s.subtrees(lambda t: t.label() == "CD"))) > 0 and not sub_text.isalpha()):
            for i in range(0, len(s)):
                s[i] = ''
    result = replaceDuplicateSequence(' '.join(tree.leaves())).replace('  ', ' ')
    ### END REMOVE UNKOWN NUMBER ###
    
    ### REMOVE PREDICATE, FULL MATCH ONLY ###
    text = result
    replacement = dict()
    for triple in triples:
        s,p,o = triple
        key, value, label = p
        key = ' '.join(camel_case_split(removeBracket(key))).replace("_", " ")
        
        value = removeBracket(value)
        replacement[key] = value
    
    for k in sorted(replacement, key=lambda k: len(k), reverse=True):
        find_idx = re.search(k.lower(), text.lower())
        if find_idx != None:
            find_idx = find_idx.start()
        else:
            continue
        end_index = find_idx+len(k)
        while end_index < len(text) and text[end_index] != ' ':
            end_index += 1
        text = text[:find_idx]+replacement[k]+text[end_index:]
    result2 = removeNonEntityBracket(replaceDuplicateSequence(text)).replace('  ', ' ')
    ### END REMOVE PREDICATE, FULL MATCH ONLY ###
    
    if(result == result2):
        return ((triples, result), None)
    else:
        return ((triples, result), (triples, result2))
        
def getSentenceFromAllTriple(triples, triple_max_len, includeLabel = True):
    sentence = ''
    for triple in triples:
        triple_part = ''
        for part in triple:
            key, value, labels = part
            label = ' '.join(labels)
            if(label=='UNKNOWN'):
                label2 = containBracket(key)
                if label2 != None:
                    label = label2
            if includeLabel:
                triple_part += _(value) + " " + _(label.lower()) + " "
            else:
                triple_part += _(key) + " "
        triple_part = word_tokenize(triple_part)
        while len(triple_part) < triple_max_len:
            triple_part.append("pad")
        triple_part = triple_part[:triple_max_len]
        sentence += ' '.join(triple_part)
        sentence += ' '
    return sentence
    
def getSentenceFromGraph(triples, triple_max_len, includeLabel = True, graph_traversal = 'toposort'):
    G = nx.DiGraph()
    first = True
    start_node = 0
    sentence = ''
    for triple in triples:
        s, p, o = triple
        key_s, value_s, label_s = s
        key_p, value_p, label_p = p
        key_o, value_o, label_o = o
        label_s = ' '.join(label_s)
        label2=None
        if(label_s=='UNKNOWN'):
            label2 = containBracket(key_s)
            if label2 != None:
                label_s = label2.lower()
        if label_p != '<unk>':
            label_p = ' '.join(label_p)
        label2=None
        if(label_p=='UNKNOWN'):
            label2 = containBracket(key_p)
            print label2
            if label2 != None:
                label_p = label2.lower()
        label_o = ' '.join(label_o)
        label2=None
        if(label_o=='UNKNOWN'):
            label2 = containBracket(key_o)
            if label2 != None:
                label_o = label2.lower()
        if includeLabel:
            sub = _(value_s + ' ' + label_s.lower())
            pre = _(label_p.lower())# +' '+value_p)
            obj = _(value_o + ' ' + label_o.lower())
        else:
            sub = _(key_s)
            pre = _(label_p)# +' '+value_p)
            obj = _(key_o)
        if first:
            start_node = sub
            first = False
        G.add_edge(sub, obj, pred=pre)
    
    max_now = -1
    for n in nx.nodes(G):
        count = 0
        for node in nx.dfs_preorder_nodes(G, n):
            count += 1
        if count > max_now:
            start_node = n
            max_now = count
    
    path_length = 0
    prev_ent = ''
    pads = []
    for i in range(0,triple_max_len):
        pads.append('pad')
    if graph_traversal == 'bfs':
        traversal = sorted(G.out_edges_iter())
    elif graph_traversal == 'dfs':
        traversal = nx.edge_dfs(G)
    else:
        traversal = sorted(getTopoSort(G.copy(), start_node))
    for x in traversal: 
        if prev_ent != _(x[0]):
            subject_string = word_tokenize(_(x[0]))
            subject_string = subject_string[:triple_max_len]
            while len(subject_string) < triple_max_len:
                subject_string.append('pad')
            sentence += ' '.join(subject_string)
            sentence += ' '
            sentence += ' '. join(pads)
            sentence += ' '
        if graph_traversal == 'bfs':
            prev_ent = _(x[0]) # CHANGE HERE, FOR BFS
        else:
            prev_ent = _(x[1]) #CHANGE HERE, FOR DFS AND TOPO
        
        object_string = word_tokenize(_(x[1]))
        object_string = object_string[:triple_max_len]
        while len(object_string) < triple_max_len:
            object_string.append('pad')
        sentence += ' '.join(object_string)
        sentence += ' '
        
        predicate_string = _(G[x[0]][x[1]]['pred']).split(' ')
        predicate_string = predicate_string[:triple_max_len]
        while len(predicate_string) < triple_max_len:
            predicate_string.append('pad')
        sentence += ' '.join(predicate_string)
        sentence += ' '
                
        path_length+=1
    return sentence
        
def getTopoSort(G, start):    
    result = list()
    scg = list(nx.strongly_connected_components(G))
    pref_candidate = start
    for i in range (0, len(scg)):
        scg = list(nx.strongly_connected_components(G))
        for j in range (0, len(scg)):
            if len(scg[j]) == 1:
                removed_node = None
                for node in scg[j]:
                    if G.in_degree(node) == 0:
                        if node == pref_candidate:
                            removed_node = node
                            break
                        else:
                            removed_node = node
                if removed_node != None:
                    for neighbor in G.neighbors(node):
                        res = res = (node, neighbor, G[node][neighbor]['pred'])
                        result.append(res)
                    G.remove_node(node)
                    break

    scg = list(nx.strongly_connected_component_subgraphs(G))
    scg.sort(key=len, reverse=True)
    for sg in scg:
        for node in nx.dfs_preorder_nodes(sg, choice(sg.nodes())):
            for neighbor in G.neighbors(node):
                res = (node, neighbor, G[node][neighbor]['pred'])
                result.append(res)
    return result
    
def createDataset(dataset, filename, triple_mode, includeLabel, triple_max_len):
    src_file = open('data/'+filename+'.src', 'w')
    tar_file = open('data/'+filename+'.tar', 'w')

    for data in dataset:
        triples, text = data
        if triple_mode == 'single':
            sentence = getSentenceFromAllTriple(triples, triple_max_len, includeLabel=includeLabel) 
        elif triple_mode == 'graph':
            sentence = getSentenceFromGraph(triples, triple_max_len, includeLabel=includeLabel)
        src_file.write(b_(sentence)+"\n")
        tar_file.write(b_(' '.join(word_tokenize(text)))+'\n')
        
    src_file.close()
    tar_file.close()

### triples -> ((S1,P1,O1), (S2,P2,O2))
### S or P or O -> (text, SUBJECT/PREDICATE/OBJECT_NUMBER, [keyword1, keyword2,...])
def transform_dataset(full_data, input_data, abbr, mode="train"): 
    idx = 1
    for entr in input_data:    
        data_triples, texts = entr

        #Determine number of subject, predicate, and object
        entities = dict()
        predicates = dict()
        num_pred = 1
        num_ent = 1
        for triple in data_triples:
            s,p,o = triple
            if p not in predicates:
                predicates[p] = num_pred
                num_pred+=1
            if s not in entities:
                entities[s] = num_ent
                num_ent+=1
            if o not in entities:
                entities[o] = num_ent
                num_ent+=1

        #Populate data
        triples = list()
        for triple in data_triples:
            s,p,o = triple

            #GET LABEL
            tmp_p = nltk.word_tokenize(p)
            p_label = list()
            for w in tmp_p:
                w_list = camel_case_split(w)
                p_label += w_list

            tmp_s = getLabelFullTries(b_(s), "UNKNOWN", dictionary=dict_label, advance=True).split('[%%%]') #CHOOSE USING DBPEDIA OR DICT
            if len(tmp_s) == 2:
                tmp_s[1] = getTypeSubject(tmp_s[1], ' '.join(p_label)).upper()
            tmp_s = nltk.word_tokenize(' '.join(tmp_s))
            s_label = list()
            for w in tmp_s:
                w_list = camel_case_split(w)
                s_label += w_list

            tmp_o = getLabelFullTries(b_(o), "UNKNOWN", dictionary=dict_label, advance=True).split('[%%%]') #CHOOSE USING DBPEDIA OR DICT
            tmp_o = nltk.word_tokenize(' '.join(tmp_o))
            o_label = list()
            for w in tmp_o:
                w_list = camel_case_split(w)
                o_label += w_list
            #END

            new_triple = [[s, "ENTITIES_"+str(entities[s]), s_label], [p, "PREDICATE_"+str(predicates[p]), p_label], [o, "ENTITIES_"+str(entities[o]), o_label]]
            triples.append(new_triple)

        if mode == "train":
            for lex in texts:
                text = lex
                text = text.replace(_('?'), ' ')
                text, triples = replaceAbbreviation(text, triples, abbr)
                data1, data2 = replaceString(text, triples)
                full_data.append(data1)

                if(data2 != None):
                    full_data.append(data2)
        else:
            text = "TEXT"
            full_data.append([triples, text])

        print idx, "of", len(input_data)
        idx+=1

def load_abbreviation():
    fo = open('data/wikipedia-acronyms.txt')

    abbr = dict()
    for line in fo:
        tokens = line.split('\t')
        abbrev = tokens[0]
        phrase = tokens[-1]
        phrase = phrase.strip()
        if abbrev not in abbr:
            abbr[abbrev] = [phrase]
        else:
            abbr[abbrev].append(phrase)

    fo.close()
    return abbr

def load_dictionary():
    fo = open('data/dictionary.txt', 'r')

    dict_label = dict()
    for line in fo:
        items = line.split('[%%%]')
        key = items[0]
        value = items[1]
        if len(items)==3:
            value2 = items[2]
        else:
            value2 = ''
        dict_label[key] = (value.strip()+'[%%%]'+value2.strip()).strip()

    fo.close()
    return dict_label
dict_label = load_dictionary()

from nltk.tokenize.moses import MosesTokenizer, MosesDetokenizer
def detokenizer(str):
    m_detokenizer = MosesDetokenizer()
    str = str.replace('- LRB-', '(')
    str = str.replace('- RRB-', ')')
    str = str.replace('-LRB-', '(')
    str = str.replace('-RRB-', ')')
    str = str.replace('`', "'")
    str = str.replace("''", "'")
    list_sent = str.split('.')
    for i in range(len(list_sent)-1,1,-1):
        if list_sent[i] == list_sent[i-1]:
            del list_sent[i]
    str = '.'.join(list_sent)
    return str.lower()
    tokens = str.split()
    result = m_detokenizer.detokenize(tokens, return_str=True)
    tokens = result.split()
    result = "".join([" "+i if (not i.startswith("'s") and not i.startswith("' ") and not i.startswith("'.")) else i for i in tokens]).strip()
    result = result.replace('( ', '(')
    return result